import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  public session = {
    username: null
  };

  constructor() { }
}
