import { TestBed, inject } from '@angular/core/testing';
import { Observable } from 'rxjs-compat';
import { InterceptorService } from './interceptor.service';

describe('InterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InterceptorService]
    });
  });

  it('should be created', inject([InterceptorService], (service: InterceptorService) => {
    expect(service).toBeTruthy();
  }));

  it('should be created', inject([InterceptorService], (service: InterceptorService) => {
    const request: any = {
      clone: function (json: any) { }
    };
    const next: any = {
      handle: function (json: any) {
        return Observable.of('');
      },
    };
    service.intercept(request, next);
    expect(service).toBeTruthy();
  }));
});
