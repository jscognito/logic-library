import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { LogoutService } from './logout.service';

describe('LogoutService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      providers: [
        LogoutService
      ]
    });
  });

  it('should be created', inject([LogoutService], (service: LogoutService) => {
    expect(service).toBeTruthy();
  }));

  it('should be set-get api login', inject( [LogoutService], (service: LogoutService) => {
    service.setApiLogout('api_logout');
    expect(service.getApiLogout()).toEqual('api_logout');
  }));

  it('should be login', inject( [LogoutService], (service: LogoutService) => {
    expect(service.logout()).toBeTruthy();
  }));
});
