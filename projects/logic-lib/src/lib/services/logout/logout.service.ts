import { Injectable } from '@angular/core';
import { HttpRequestService } from '../httpRequest/http-request.service';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  private API_LOGOUT: string;

  constructor(public httpRequestService: HttpRequestService) { }

  logout(): Observable<any> {
    const type = 'POST';
    const url = this.API_LOGOUT;
    const body = null;
    const httpParams = null;
    return this.httpRequestService.httpRequest(type, url, body, httpParams);
  }

  getApiLogout() {
    return this.API_LOGOUT;
  }

  setApiLogout(API_LOGIN: string) {
    this.API_LOGOUT = API_LOGIN;
  }
}
