import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {

  constructor(public httpClient: HttpClient) { }

  public httpRequest(typeRequest: string, url: string, bodyRequest: any, queryString: HttpParams): Observable <any>  {

    
    return this.httpClient.request(typeRequest, url, { body: bodyRequest, observe: 'response', params: queryString });
  }

}
