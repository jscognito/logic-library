import { Injectable } from '@angular/core';
import { HttpRequestService } from '../httpRequest/http-request.service';
import { Observable } from 'rxjs/internal/Observable';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private API_LOGIN: string;

  constructor(public httpRequestService: HttpRequestService) { }

  login(bodyRequest: any): Observable<any> {
    const type = 'POST';
    const url = this.API_LOGIN;
    const body = bodyRequest;
    const httpParams = null;
    return this.httpRequestService.httpRequest(type, url, body, httpParams);
  }

  getApiLogin() {
    return this.API_LOGIN;
  }

  setApiLogin(API_LOGIN: string) {
    this.API_LOGIN = API_LOGIN;
  }
}
