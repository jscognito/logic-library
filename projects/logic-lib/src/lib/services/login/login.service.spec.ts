import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { LoginService } from './login.service';

describe('LoginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      providers: [
        LoginService
      ]
    });
  });

  it('should be created', inject([LoginService], (service: LoginService) => {
    expect(service).toBeTruthy();
  }));

  it('should be set-get api login', inject( [LoginService], (service: LoginService) => {
    service.setApiLogin('api_login');
    expect(service.getApiLogin()).toEqual('api_login');
  }));

  it('should be login', inject( [LoginService], (service: LoginService) => {
    const bodyRequest: any = 'client_id=' + '' + '&username=' + '' + '&password=' + '' + '&grant_type=password';
    expect(service.login(bodyRequest)).toBeTruthy();
  }));

});
