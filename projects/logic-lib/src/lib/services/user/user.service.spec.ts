import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user.service';
import { userInfo } from 'os';

describe('UserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      providers: [
        UserService
      ]
    });
  });

  it('should be created', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));

  it('should be set-get api user', inject( [UserService], (service: UserService) => {
    service.setApiUser('api_user');
    expect(service.getApiUser()).toEqual('api_user');
  }));

  it('should be login', inject( [UserService], (service: UserService) => {
    const username = 'username';
    expect(service.veryfyUser(username)).toBeTruthy();
  }));
});
