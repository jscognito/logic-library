import { Injectable } from '@angular/core';
import { HttpRequestService } from '../httpRequest/http-request.service';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private API_USER: string;

  constructor(
    public httpRequestService: HttpRequestService
  ) { }

  veryfyUser(username: any): Observable<any> {
    const type = 'GET';
    const url = this.API_USER + username;
    const body = null;
    const httpParams = null;
    return this.httpRequestService.httpRequest(type, url, body, httpParams);
  }

  getApiUser() {
    return this.API_USER;
  }

  setApiUser(API_USER: string) {
    this.API_USER = API_USER;
  }
}
