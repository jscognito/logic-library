import { TestBed, inject } from '@angular/core/testing';

import { LogicLibService } from './logic-lib.service';

describe('LogicLibService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogicLibService]
    });
  });

  it('should be created', inject([LogicLibService], (service: LogicLibService) => {
    expect(service).toBeTruthy();
  }));
});
