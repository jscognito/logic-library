import { Component } from '@angular/core';
import { ConfigService } from 'logic-lib';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'logic-library';

  constructor(
    public configService: ConfigService
  ) {
    this.configService.config.channel.clientId = 'demoEtapaTres';
    this.configService.config.channel.secret = btoa(this.configService.config.channel.clientId + ':' + 'NdK4sJhffsRnA');
  }
}
