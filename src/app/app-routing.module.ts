import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { GettingStartedComponent } from './pages/getting-started/getting-started.component';
import { ConfigComponent} from './pages/config/config.component';
import { SessionComponent } from './pages/session/session.component';
import { InterceptorComponent } from './pages/interceptor/interceptor.component';
import { LoginComponent} from './pages/login/login.component';
import { LogoutComponent} from './pages/logout/logout.component';
import { UserComponent} from './pages/user/user.component';

const routes: Routes = [
  { path: 'getting-started', component: GettingStartedComponent },
  { path: 'config', component: ConfigComponent },
  { path: 'session', component: SessionComponent },
  { path: 'interceptor', component: InterceptorComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'user', component: UserComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes), CommonModule ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
