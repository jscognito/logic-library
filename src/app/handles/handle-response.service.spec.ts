import { TestBed, inject } from '@angular/core/testing';

import { HandleResponseService } from './handle-response.service';

describe('HandleResponseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HandleResponseService]
    });
  });

  it('should be created', inject([HandleResponseService], (service: HandleResponseService) => {
    expect(service).toBeTruthy();
  }));
});
