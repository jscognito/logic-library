import { Component, OnInit } from '@angular/core';
import { UserService } from 'logic-lib';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public API_USER = '';

  code = `
  import { Component, OnInit } from '@angular/core';
  import { UserService } from 'logic-lib';
  
  @Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
  })
  export class UserComponent implements OnInit {
  
    constructor(
      public userService: UserService
    ) { }
  }
    `;

  constructor(public userService: UserService) { }

  ngOnInit() {
  }

}
