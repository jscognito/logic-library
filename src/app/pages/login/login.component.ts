import { Component, OnInit } from '@angular/core';
import { HighlightResult } from 'ngx-highlightjs';
import { ConfigService, LoginService } from 'logic-lib';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public API_LOGIN = '';

  code = `
  import { Component, OnInit } from '@angular/core';
  import { LoginService } from 'logic-lib';
  
  @Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
  })
  export class LoginComponent implements OnInit {
    constructor(
      public loginService: LoginService) {
    }

    public login(username: string, password: string) {
      this.loginService.setApiLogin(this.API_LOGIN);
      // tslint:disable-next-line:max-line-length
      const bodyRequest: any = 'client_id=' + this.configService.config.channel.clientId +
      '&username=' + username + '&password=' + password + '&grant_type=password';
      this.loginService.login(bodyRequest).subscribe(
        response => {
          console.log(response);
          this.configService.config.token.value = response.body.access_token;
          this.configService.config.token.type = response.body.token_type;
  
        }, error => {
          console.log(error);
        });
    }
  }`;

  constructor(
    public configService: ConfigService,
    public loginService: LoginService) {
  }

  ngOnInit() {
  }

}
