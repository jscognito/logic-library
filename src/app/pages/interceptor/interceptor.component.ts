import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-interceptor',
  templateUrl: './interceptor.component.html',
  styleUrls: ['./interceptor.component.css']
})
export class InterceptorComponent implements OnInit {

  code = `
  import { Injectable } from '@angular/core';
  import { HttpInterceptor } from '@angular/common/http/src/interceptor';
  import { HttpRequest, HttpHandler, HttpEvent, } from '@angular/common/http';
  import { Observable } from 'rxjs';
  import 'rxjs/add/operator/do';
  import 'rxjs/add/operator/catch';
  import 'rxjs/Observable';
  import 'rxjs/add/observable/throw';
  import { ConfigService } from '../config/config.service';
  import { SessionService } from '../session/session.service';
  
  @Injectable({
    providedIn: 'root'
  })
  export class InterceptorService implements HttpInterceptor {
  
  
    constructor(
      public configService: ConfigService,
      public sessionService: SessionService ) {
    }
  
    /**
     * Método interceptor
     * @param request 
     * @param next 
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      let authorization;
      let contentType;
  
      if (this.configService.config.token.value === null) {
        authorization = 'Basic' + ' ' + this.configService.config.channel.secret;
        contentType = 'application/x-www-form-urlencoded';
      } else {
        authorization = this.configService.config.token.type + ' ' + this.configService.config.token.value;
        contentType = 'application/json';
      }
  
      request = request.clone({
        setHeaders: {
          'Authorization': '' + authorization,
          'Content-Type': '' + contentType,
          'Audit': this.sessionService.session.username === null ? '' : '' + this.sessionService.session.username
        }
      });
      return next.handle(request)
        .catch(error => {
          {
            return this.handle400Error(error);
          }
        });
    }
   
    handle400Error(error) {
      try {
        return Observable.throw(error);
      } catch (e) {
        return Observable.throw(error);
      }
  
    }
  
  }
  `;

  constructor() { }

  ngOnInit() {
  }

}
