import { Component, OnInit } from '@angular/core';
import { SessionService } from 'logic-lib';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})
export class SessionComponent implements OnInit {

  code = `
  import { Component, OnInit } from '@angular/core';
  import { SessionService } from 'logic-lib';

  @Component({
    selector: 'app-session',
    templateUrl: './session.component.html',
    styleUrls: ['./session.component.css']
  })
  export class SessionComponent implements OnInit {

  constructor(public sessionService: SessionService) { }

  }
  `;

  constructor(public sessionService: SessionService) { }

  ngOnInit() {
  }

}
