import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'logic-lib';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit {

  code = `
  import { Component, OnInit } from '@angular/core';
  import { ConfigService } from 'logic-lib';

  @Component({
    selector: 'app-config',
    templateUrl: './config.component.html',
    styleUrls: ['./config.component.css']
  })
  export class ConfigComponent implements OnInit {

  constructor(public configService: ConfigService) {
      //this.configService.config: {
      // token: {
      //     type: any;
      //     value: any;
      // };
      // channel: {
      //     clientId: any;
      //     grantType: any;
      //     secret: any;
      // };
      //};
  }

  }
  `;

  public config;

  constructor(public configService: ConfigService) {
    this.config = this.configService.config;
  }

  ngOnInit() {
  }

}
