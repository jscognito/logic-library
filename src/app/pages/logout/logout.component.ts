import { Component, OnInit } from '@angular/core';
import { LogoutService, ConfigService } from 'logic-lib';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  public API_LOGOUT = '';

  code = `
  import { Component, OnInit } from '@angular/core';
  import { LogoutService } from 'logic-lib';
  
  @Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html',
    styleUrls: ['./logout.component.css']
  })
  export class LogoutComponent implements OnInit {
    constructor(
      public logoutService: LogoutService
    ) { }

    public logout() {
      this.logoutService.setApiLogout(this.API_LOGOUT);
      this.logoutService.logout().subscribe(
        response => {
          console.log(response);
          this.configService.config.token.value = null;
          this.configService.config.token.type = null;
  
        }, error => {
          console.log(error);
        });
    }
  }
  `;

  constructor(
    public logoutService: LogoutService,
    public configService: ConfigService
  ) { }

  ngOnInit() {
  }

}
