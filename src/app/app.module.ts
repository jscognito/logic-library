import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { InterceptorService } from 'logic-lib';
import { HttpModule} from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { HighlightModule } from 'ngx-highlightjs';
import { LoginComponent } from './pages/login/login.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { UserComponent } from './pages/user/user.component';
import { AppRoutingModule } from './app-routing.module';
import { GettingStartedComponent } from './pages/getting-started/getting-started.component';
import { ConfigComponent } from './pages/config/config.component';
import { SessionComponent } from './pages/session/session.component';
import { InterceptorComponent } from './pages/interceptor/interceptor.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    UserComponent,
    GettingStartedComponent,
    ConfigComponent,
    SessionComponent,
    InterceptorComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    HighlightModule.forRoot({ theme: 'monokai'})
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
